package classes;

public class ClassObject {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 Car mycar=new Car("y car");
 Car anothercar=new Car("z car");

 mycar.acclerate();
 mycar.acclerate();
 mycar.acclerate();
 mycar.acclerate();
 mycar.acclerate();
 mycar.brake();
 mycar.acclerate();
 anothercar.brake();
	}

}
 
class Car{
	
	private int speed=0;
	private String name;
	Car(String name){
		this.name=name;
	}
	public void acclerate() {
		speed++;
		System.out.printf("%s is going %s kmph%n",name,speed);
	}
	public void brake() {
		speed--;
		System.out.printf("%s is going %s kmph %n",name,speed);
	}
}