package loops;

import java.util.Scanner;

public class WhileLoop {
	public static void main(String[] args) {
		String user_name="";
		Scanner sc=new Scanner(System.in);
		System.out.println("please choose one of the following option");
		System.out.println("1.cappucino");
		System.out.println("2.latte");
		System.out.println("3.milk");
		System.out.println("4.tea");
		System.out.println("Q.Quit");
		do {
			user_name=sc.nextLine().toLowerCase();
			System.out.println("you have choosen "+user_name);

			switch (user_name) {
			case "1":System.out.println("making cappucino");

			break;
			case "2":System.out.println("making latte");
			break;
			case "3":System.out.println("making milk");
			break;
			case "4":System.out.println("making tea");
			break;
			case "q":System.out.println("quit");
			break;
			default:System.out.println("you have choosen invalid option");
			break;
			}
		}
		while(!user_name.equals("q")) ;


		sc.close();
	}
}
