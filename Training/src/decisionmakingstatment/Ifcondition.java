package decisionmakingstatment;


import java.util.Random;
import java.util.Scanner;

public class Ifcondition {
	static int rock=0;
	static int paper=1;
	static int scissor=2;
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
  Random rm=new Random();
  int cmpvalue=rm.nextInt(3);
	String playerchoice;
	int playervalue=-1;
	System.out.println("enter rock,paper(or) scissor");
	playerchoice=sc.nextLine().toLowerCase();
	if(playerchoice.equals("rock")) {
		playervalue=rock;
	}
	else if(playerchoice.equals("paper")) {
		playervalue=paper;
		
	}
	else if(playerchoice.equals("scissor")) {
		playervalue=scissor;
	}
	else {
		System.out.printf("%s is invalid%n", playervalue);
	}
	System.out.printf("comp value%s player value%s.%n",cmpvalue,playervalue);
	if(playervalue==cmpvalue) {
		System.out.println("draw of the match");
	}
	else if((playervalue-1==cmpvalue)||(playervalue==rock && cmpvalue==scissor)) {
		System.out.println("player wins");
	}
	else {
		System.out.println("computer wins");
	}
}
}
